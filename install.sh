#!/bin/bash

echo "Installing expensert..."

cp -TR src /usr/local/lib/expensert/

echo "expensert lib was installed."

ln -sf /usr/local/lib/expensert/expensert /usr/local/bin/expensert

echo "expensert binary was linked successfully"

echo "expensert was installed successfully"
